Sign Up in GitLab
Create git repository in Gitlab (name � demo_Name_Surname, Visibility Level � public, Initialize repository with a README - true)
Create �develop� branch
Create 1 commit in �develop� branch
Merge �develop� branch to �master� branch
Create 1 more commit in �develop� branch
Revert commit
Emulate �merge conflict� situation and resolve it
Send me the link to your project
